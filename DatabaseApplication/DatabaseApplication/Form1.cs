﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace DatabaseApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //load data into datagridview

            LoadData();
            LoadDataIntoGridView();
            LoadDataIntoComboBox();
            LoadDataIntoTextBoxes();
            LoadDataIntoRichTextBox();

        }

        private void LoadData()
        {
            String connectionString = "datasource=localhost;port=3306;username=root";
            MySqlConnection connectionDataBase = new MySqlConnection(connectionString);
            MySqlCommand connectionCommand = new MySqlCommand("select EmpFirstName from sampledata.employee;", connectionDataBase);
            MySqlDataReader myReader;

            try
            {
                connectionDataBase.Open();
                myReader = connectionCommand.ExecuteReader();
                while (myReader.Read())
                {
                    textBox1.Text = (myReader.GetString(0));
                    //textBox1->Text += (myReader->GetString(0));
                    //textBox1->Text = (myReader->GetInt32(0));
                }
                connectionDataBase.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Oops.");
            }
        }

        private void LoadDataIntoGridView()
        {
            String connectionString = "datasource=localhost;port=3306;username=root";
            MySqlConnection connectionDataBase = new MySqlConnection(connectionString);
            MySqlCommand connectionCommand = new MySqlCommand("select * from sampledata.employee;", connectionDataBase);

            try
            {
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
                dataAdapter.SelectCommand = connectionCommand;
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
                dataGridView1.DataSource = dataTable;
                connectionDataBase.Close();
            }
            catch (Exception)
            {
                //do nothing?? Messagebox?
                MessageBox.Show("Whoops.");
            }
        }

        private void LoadDataIntoComboBox()
        {
            String connectionString = "datasource=localhost;port=3306;username=root";
            MySqlConnection connectionDataBase = new MySqlConnection(connectionString);
            MySqlCommand connectionCommand = new MySqlCommand("select * from sampledata.employee;", connectionDataBase);
            MySqlDataReader myReader;

            try
            {
                connectionDataBase.Open();
                myReader = connectionCommand.ExecuteReader();
                while (myReader.Read())
                {
                    String name = "";
                    name += myReader.GetString(1);
                    name += " " + myReader.GetString(2);

                    comboBox1.Items.Add(name);

                }
                connectionDataBase.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Oops.");
            }
        }

        private void LoadDataIntoTextBoxes()
        {
            String connectionString = "datasource=localhost;port=3306;username=root";
            MySqlConnection connectionDataBase = new MySqlConnection(connectionString);
            MySqlCommand connectionCommand = new MySqlCommand("select * from sampledata.employee;", connectionDataBase);
            MySqlDataReader myReader;

            try
            {
                connectionDataBase.Open();
                myReader = connectionCommand.ExecuteReader();
                while (myReader.Read())
                {
                    nameTextBox.Text = myReader.GetString(1);
                    surnameTextBox.Text = myReader.GetString(2);
                }
                connectionDataBase.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Oops.");
            }
        }

        private void InsertRecord(object sender, EventArgs e)
        {
            String name = nameTextBox.Text;
            String surname = surnameTextBox.Text;
            String password = passwordTextBox.Text;

            String query = "INSERT INTO SAMPLEDATA.EMPLOYEE(EmpFirstName, EmpSurname, EmpPassword) VALUES(@Name, @Surname, @Password);";
            String connectionString = "datasource=localhost;port=3306;username=root;";

            MySqlConnection conn = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand(query, conn);     


            try
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Surname", surname);
                cmd.Parameters.AddWithValue("@Password", password);

                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadDataIntoRichTextBox()
        {
            String query = "select * from sampledata.employee WHERE EmpId = 3;";
            String connectionString = "datasource=localhost;port=3306;username=root;";

            MySqlConnection conn = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataReader myReader;

            try
            {
                conn.Open();
                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    int id = myReader.GetInt32(0);
                    String name = myReader.GetString(1);
                    String surname = myReader.GetString(2);
                    String password = myReader.GetString(3);
                    DateTime startDate = myReader.GetDateTime(4);

                    Employee emp = new Employee(id, name, surname, password, startDate);
                    richTextBox1.Text = emp.ToString();
                }
                conn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Oops.");
            }
        }

        private void DeleteRecord(object sender, EventArgs e)
        {
            String name = comboBox1.SelectedText;
            //String query = "DELETE from sampledata.employee WHERE EmpId = 3;";
            String query = "DELETE from sampledata.employee WHERE EmpName = " + name + ";";
            String connectionString = "datasource=localhost;port=3306;username=root;";

            MySqlConnection conn = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Oops.");
            }
        }
    }
}
