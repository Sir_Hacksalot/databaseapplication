﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication
{
    class Employee
    {
        int id;
        String name;
        String surname;
        String password;
        DateTime startDate;

        public Employee(int id, String name, String surname, String password, DateTime startDate)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.password = password;
            this.startDate = startDate;
        }

        public override string ToString()
        {
            return "Id: " + id +
                    "\nName: " + name +
                    "\nSurname: " + surname +
                    "\nPassword: " + password +
                    "\nStart Date: " + startDate.ToLongDateString();
        }
    }
}
